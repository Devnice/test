<?php

namespace AppBundle\Controller;

use AppBundle\Crawler\Site as CrawlerSite;
use AppBundle\Entity\Site;
use AppBundle\Form\SiteAddType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class CrawlerController extends Controller
{
    /**
     * @Route("/", name="site-home")
     */
    public function indexAction()
    {
        $formUpdates = array();
        $sites = $this->getDoctrine()->getRepository('AppBundle:Site')->findAll();
        $formAdd = $this->createForm('app_form_site_add')->createView();

        foreach($sites as $site){
            $formUpdates[] = $this->createForm('app_form_site_update', $site)->createView();
        }

        return $this->render('AppBundle::index.html.twig', array(
            'formAdd' => $formAdd,
            'formUpdates' => $formUpdates
        ));
    }

    /**
     * @Route("/add", name="site-add")
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function addAction(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();

        $formAdd = $this->createForm('app_form_site_add');
        $formAdd->handleRequest($request);

        if($formAdd->isValid()){
            $url = $formAdd->getData()['url'];
            $crawlerSite = new CrawlerSite($url);

            $site = new Site();
            $site->setUrl($url);
            $site->setTitle($crawlerSite->getTitle());
            $site->setDescription($crawlerSite->getDescription());

            $manager->persist($site);
            $manager->flush();
        }

        return new RedirectResponse($this->generateUrl('site-home'));
    }

    /**
     * @Route("/update/{id}", name="site-update", requirements={"id" = "\d+"})
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function updateAction(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();

        $formUpdate = $this->createForm('app_form_site_update', $this->getDoctrine()->getRepository('AppBundle:Site')->find($request->get('id')));
        $formUpdate->handleRequest($request);

        if($formUpdate->isValid()){
            $site = $formUpdate->getData();
            $manager->persist($site);
            $manager->flush();
        }

        return new RedirectResponse($this->generateUrl('site-home'));
    }

    /**
     * @Route("/delete/{id}", name="site-delete", requirements={"id" = "\d+"})
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function deleteAction(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();

        $site = $this->getDoctrine()->getRepository('AppBundle:Site')->find($request->get('id'));

        $manager->remove($site);
        $manager->flush();

        return new RedirectResponse($this->generateUrl('site-home'));
    }
}
