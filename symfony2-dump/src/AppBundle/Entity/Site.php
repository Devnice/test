<?php
/**
 * Created by PhpStorm.
 * User: DEVNICE
 * Date: 6/2/2015
 * Time: 3:12 PM
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Page
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="page")
 * @ORM\Entity("AppBundle\Repository\SiteRepository")
 */
class Site {

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="url", type="string", length=2000)
     */
    private $url;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=100, nullable=true)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(name="description", type="string", length=200, nullable=true)
     */
    private $description;


    /**
     * Retourne l'identifiant
     * @return integer
     */
    public function getId(){
        return $this->id;
    }


    /**
     * Retourne l'URL
     * @return string
     */
    public function getUrl(){
        return $this->url;
    }

    /**
     * Fixe la valeur de la propriété "url"
     * @param string $url
     * @return $this
     */
    public function setUrl($url){
        $this->url = $url;
        return $this;
    }

    /**
     * Retourne le titre
     * @return string
     */
    public function getTitle(){
        return $this->title;
    }

    /**
     * Fixe la valeur de la propriété "title"
     * @param string $title
     * @return $this
     */
    public function setTitle($title){
        $this->title = $title;
        return $this;
    }

    /**
     * Retourne la description
     * @return string
     */
    public function getDescription(){
        return $this->description;
    }

    /**
     * Fixe la valeur de la propriété "description"
     * @param string $description
     * @return $this
     */
    public function setDescription($description){
        $this->description = $description;
        return $this;
    }
}