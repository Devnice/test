<form class="form-update" action="index.php" method="post">
    <label for="title">{$data.url} :</label>
    <input type="hidden" name="id" value="{$data.id}">
    <input type="text" name="title" maxlength="100" value="{$data.title}" placeholder="Titre">
    <textarea name="description" maxlength="200" placeholder="Description">{$data.description}</textarea>
    <button type="submit" name="action" value="update">Modifier</button>
    <button type="submit" name="action" value="delete">Supprimer</button>
</form>