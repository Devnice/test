<html>
    <head>
        <title><?php echo $title ?></title>
        <meta charset="UTF-8" />
        <style type="text/css">
            * {
                box-sizing: border-box;
            }

            label, input, textarea, button {
                width: 100%;
                display: block;
                font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
                font-size: 14px;
            }

            label {
                margin: 10px 0 0;
                padding: 6px 0;
                font-weight: bold;
            }

            input, textarea, button {
                margin:2px 0;
                padding: 6px 12px;
                border:1px solid gray;
            }

            button {
                font-weight: bold;
            }
            button[value="add"] {
                background-color: darkseagreen;
            }
            button[value="update"] {
                background-color: steelblue;
                color: white;
            }
            button[value="delete"] {
                background-color: indianred;
                color: white;
            }

            .alert-error {
                width: 100%;
                display: block;
                font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
                font-size: 18px;
                padding: 6px 0;
                color: indianred;
            }
        </style>
    </head>

    <body>

    <?php
        $errors = validation_errors();
        if(false === empty($errors)) {
            echo '<div class="alert-error">'.validation_errors().'</div>';
        }
    ?>

        <h3>Ajouter une nouvelle URL</h3>
        <form id="form-add" action="<?php echo base_url() ?>index.php/app/add" method="post">
            <input type="url" name="url" required="true" placeholder="Lien Hypertext (URL)"/>
            <button type="submit" name="action" value="add">Ajouter</button>
        </form>

        <h3>URLs en base</h3>
        <?php echo $items ?>
    </body>
</html>