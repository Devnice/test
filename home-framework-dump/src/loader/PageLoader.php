<?php

/**
 * Class PageLoader
 * Permet de charger et de convertir une page xhtml en objet
 * et dans récupérer certains informations
 */
class PageLoader
{
    /**
     * @var DOMXPath
     */
    private $dom;


    /**
     * @param string $url
     */
    public function __construct($url) {
        $this->check($url);

        libxml_use_internal_errors(true);
        $document = new DOMDocument();
        $document->loadHTML(file_get_contents($url));
        $this->dom = new DOMXPath($document);
    }

    /**
     * Retourne la description de la page
     * @return string|null
     */
    public function getTitle() {
        $elem = $this->dom->query("//html/head/title")->item(0);
        return ($elem) ? $elem->nodeValue : null;
    }

    /**
     * Retourne la description de la page
     * @return string|null
     */
    public function getDescription() {
        $elem = $this->dom->query("//html/head/meta[@name='description']")->item(0);
        if($elem) $elem = $elem->attributes->getNamedItem('content');
        return ($elem) ? $elem->nodeValue : null;
    }

    /**
     * Permet de savoir si l'url est accessible
     * @param $url
     */
    private function check($url) {
        if(!is_url($url)) throw new UnexpectedValueException('Url non conforme');

        @$headers = get_headers($url);
        if(!preg_match('/^HTTP\/\d.\d\s+(200|301|302)/', $headers[0])) {
            throw new RuntimeException("La page n'est pas accessible");
        }
    }
}