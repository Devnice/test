<?php

require_once DIR_COMPOSANTS.'/Connection.php';


/**
 * Class Page
 * Permet de gérér les données en base sur les informtions récuperées
 * des pages web
 */
class Page
{
    const TABLE = 'page';

    /**
     * Ajoute un nouvel enregistrement en base
     * @param string $url
     * @param string|null $title
     * @param string|null $description
     */
    public static function add($url, $title = null, $description = null)
    {
        $con = Connection::getInstance();

        if(
            !$con->exec(sprintf(
                "INSERT INTO `%s` (`url`, `title`, `description`) VALUES (%s, %s, %s)",
                static::TABLE,
                Connection::secure($url),
                Connection::secure($title),
                Connection::secure($description)
            ))
        ){
            throw new LogicException(sprintf("%s : %s", __METHOD__, Connection::getInstance()->errorInfo()[2]));
        }
    }

    /**
     * Modifie un enregistrement existant en base
     * @param int $id
     * @param string|null $title
     * @param string| null $description
     */
    public static function update($id, $title = null, $description = null)
    {
        $con = Connection::getInstance();

        if(
            !$con->exec(sprintf(
                "UPDATE `%s` SET `title` = %s, `description` = %s WHERE `id` = %s",
                static::TABLE,
                Connection::secure($title),
                Connection::secure($description),
                Connection::secure($id)
            ))
        ){
            throw new LogicException(sprintf("%s : %s", __METHOD__, Connection::getInstance()->errorInfo()[2]));
        }
    }

    /**
     * Supprime un enregistrement existant an base
     * @param $id
     * @return boolean
     */
    public static function delete($id)
    {
        $con = Connection::getInstance();

        if(
            !$con->exec(sprintf(
                "DELETE FROM `%s` WHERE `id` = %s",
                static::TABLE,
                Connection::secure($id)
            ))
        ){
            throw new LogicException(sprintf("%s : %s", __METHOD__, Connection::getInstance()->errorInfo()[2]));
        }
    }

    /**
     * Retourne tous les enregistrement exitants en base
     * @return array
     */
    public static function all()
    {
        $con = Connection::getInstance();

        $ret = $con->query(sprintf(
            "SELECT * FROM `%s`",
            static::TABLE
        ));

        return (false !== $ret) ? $ret->fetchAll(PDO::FETCH_ASSOC) : array();
    }
}