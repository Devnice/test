<?php

/**
 * Permet de savoir une url est conforme
 * @param string $url
 * @return boolean
 */
function is_url($url) {
    $pattern='|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i';
    if(preg_match($pattern, $url)) return true;
    else return false;
}