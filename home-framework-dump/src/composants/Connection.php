<?php

class Connection {

    /**
     * Instance de la classe PDO
     * @var PDO
     */
    private static $_instance;


    /**
     * Créer et retourne une instance de connexion
     * @return PDO
     */
    public static function getInstance(){
        if(is_null(self::$_instance)) {
            self::$_instance = new PDO(
                sprintf('%s:host=%s;dbname=%s', DATABASE_DRIVER, DATABASE_HOST, DATABASE_NAME),
                DATABASE_USER,
                DATABASE_PASSWORD,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8')
            );
        }

        return self::$_instance;
    }


    /**
     * Permet de protéger une valeur utiliser dans une rêquete SQL
     * et éviter tout debordant
     * @param string|null $value
     * @return string
     */
    public static function secure($value)
    {
        if(is_null(self::$_instance)) {
            throw new LogicException("Aucune connexion n'a encore été initialisée");
        }

        $test = (is_string($value)) ? str_replace(' ', '', $value) : $value;

        if($test === '' || is_null($test)) {
            $value = 'DEFAULT';
        }
        else {
            $value = self::$_instance->quote($value);
        }

        return $value;
    }
}