<?php

# Application Title
define('NAME_APP', 'Dump');

# Connection parameters of the database
define('DATABASE_DRIVER', 'mysql');
define('DATABASE_HOST', 'localhost');
define('DATABASE_NAME', 'dump');
define('DATABASE_USER', 'root');
define('DATABASE_PASSWORD', null);

# Application directory
define('DIR_ROOT', rtrim(dirname(str_replace(DIRECTORY_SEPARATOR, '/', __DIR__)),'/'));
define('DIR_LIBS', DIR_ROOT.'/libs');
define('DIR_CACHE', DIR_ROOT.'/cache');
define('DIR_COMPOSANTS', DIR_ROOT.'/src/composants');
define('DIR_LOADER', DIR_ROOT.'/src/loader');
define('DIR_CONTROLLER', DIR_ROOT.'/src/controller');
define('DIR_MODELE', DIR_ROOT.'/src/modele');
define('DIR_VIEW', DIR_ROOT.'/src/view');