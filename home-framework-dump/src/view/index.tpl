<!DOCTYPE html>

<html>
    <head>
        <title>Agrégateur</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" charset="UTF-8" media="all" href="public/css/base.css">
    </head>
    <body>
        <div id="wrap">
            {include file='alert.tpl'}

            <h3>Ajouter une nouvelle URL</h3>
            <form id="form-add" action="index.php" method="post">
                <input type="url" name="url" required="true" placeholder="Lien Hypertext (URL)"/>
                <button type="submit" name="action" value="add">Ajouter</button>
            </form>

            <h3>URLs en base</h3>
            {foreach $pages as $row}
                {include file='page.tpl' data=$row}
            {/foreach}
        </div>
    </body>
</html>