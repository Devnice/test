<?php


/**
 * Class Pages
 * Controller WebSite Copier
 */
class App extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('language','form','url'));
        $this->load->model('site_model');
        $this->load->library('form_validation');
        $this->lang->load('base');
    }


    public function index()
    {
        $this->load->view('app.php', array(
            'title' => lang('base_title'),
            'items' => $this->load->view('items.php', array(
                'data' => $this->site_model->all()
            ), true)
        ));
    }


    public function add()
    {
        $this->form_validation->set_rules('url', 'URL', 'required|callback_is_url|callback_url_exist');
        if($this->form_validation->run() === true){
            $url = $this->input->post('url');
            $info = $this->site_info($url);
            var_dump($info);

            $this->site_model->add(
                $url,
                $info['title'],
                $info['description']
            );
        }

        $this->index();
    }


    public function update()
    {
        $this->form_validation->set_rules('id', 'ID', 'required|integer');
        if($this->form_validation->run() === true){
            $this->site_model->update(
                $this->input->post('id'),
                $this->input->post('title'),
                $this->input->post('description')
            );
        }

        $this->index();
    }


    public function delete()
    {
        $this->form_validation->set_rules('id', 'ID', 'required|integer');
        if($this->form_validation->run() === true){
            $this->site_model->delete(
                $this->input->post('id')
            );
        }

        $this->index();
    }

    /**
     * @see http://garridodiaz.com/check-if-url-exists-and-is-online-php/
     * @param $url
     * @return bool
     */
    public function is_url($url)
    {
        $pattern='|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i';
        if(preg_match($pattern, $url)) {
            return TRUE;
        }
        else {
            $this->form_validation->set_message('is_url', 'The %s field is not an URL');
            return FALSE;
        }
    }


    /**
     * @see http://garridodiaz.com/check-if-url-exists-and-is-online-php/
     * @param $url
     * @return bool
     */
    public function url_exist($url)
    {
        @$headers = get_headers($url);
        if(!preg_match('/^HTTP\/\d.\d\s+(200|301|302)/', $headers[0])) {
            return false;
        }
        else {
            return true;
        }
    }


    public function site_info($url)
    {
        $info = array();

        libxml_use_internal_errors(true);
        $document = new DOMDocument();
        $document->loadHTML(file_get_contents($url));
        $this->dom = new DOMXPath($document);

        $elem = $this->dom->query("//html/head/title")->item(0);
        if($elem) {
            $info['title'] = $elem->nodeValue;
        }
        else {
            $info['title'] = null;
        }

        $elem = $this->dom->query("//html/head/meta[@name='description']")->item(0);
        if($elem) $elem = $elem->attributes->getNamedItem('content');
        if($elem) {
            $info['description'] = $elem->nodeValue;
        }
        else {
            $info['description'] = null;
        }

        return $info;
    }
}