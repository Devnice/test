<?php
require_once 'config/config.inc.php';
require_once DIR_COMPOSANTS.'/fonctions.php';
require_once DIR_CONTROLLER.'/PageController.php';


//$loader = new PageLoader('http://www.batimedia.com');
//var_dump($loader->getTitle());
//var_dump($loader->getDescription());


//    Page::add('http://www.url1.com', 'title1', 'description1');
//    Page::add('http://www.url2.com', 'title2', 'description2');
//    Page::add('http://www.url3.com', 'title3', 'description3');
//    Page::add('http://www.url4.com', '', null);
//
//
//    Page::update(3, 'http://www.url5.com', 'title5', 'description5');
//
//    Page::delete(2);
//
//    var_dump(Page::all());




$controller = new PageController();
$action = (isset($_POST['action']) && !empty($_POST['action'])) ? $_POST['action']  : null;

if($action == 'add') {
    $response = $controller->addAction($_POST['url']);
}
else if($action == 'update') {
    $response = $controller->updateAction($_POST['id'], $_POST['title'], $_POST['description']);
}
else if($action == 'delete') {
    $response = $controller->deleteAction($_POST['id']);
}
else {
    $response = $controller->indexAction();
}

echo $response;
