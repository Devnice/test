<?php

require_once DIR_LIBS.'/smarty/libs/Smarty.class.php';

/**
 * Permet de définir un classe Controlleur
 * Class Controller
 */
abstract class Controller extends Smarty
{

    public function __construct(){
        parent::__construct();

        // Configuration propre à Smarty
        $this->template_dir = DIR_VIEW;
        $this->compile_dir = DIR_CACHE.'/smarty/templates_c/';
        $this->cache_dir = DIR_CACHE.'/smarty/cache/';
    }
}