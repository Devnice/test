<?php
/**
 * Created by PhpStorm.
 * User: DEVNICE
 * Date: 6/2/2015
 * Time: 5:16 PM
 */
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SiteAddType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('url', 'text', array(
                'attr' => array(
                    'placeholder' => 'URL'
                )
            ))
            ->add('btn_add', 'submit', array(
                'label' => 'Ajouter',
                'attr' => array(
                    'class' => 'btn-add'
                )
            ));
    }


    public function getName()
    {
        return 'app_form_site_add';
    }
}