<?php
/**
 * Created by PhpStorm.
 * User: DEVNICE
 * Date: 6/2/2015
 * Time: 5:16 PM
 */
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SiteUpdateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'attr' => array(
                    'placeholder' => 'Titre'
                ),
                'required' => false
            ))
            ->add('description', 'textarea', array(
                'attr' => array(
                    'placeholder' => 'Description'
                ),
                'required' => false
            ))
            ->add('btn_update', 'submit', array(
                'label' => 'Modifier',
                'attr' => array(
                    'class' => 'btn-update'
                )
            ));
    }


    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Site'
        ));
    }


    public function getName()
    {
        return 'app_form_site_update';
    }
}