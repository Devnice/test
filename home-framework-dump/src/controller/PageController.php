<?php
require_once DIR_COMPOSANTS.'/Controller.php';
require_once DIR_LOADER.'/PageLoader.php';
require_once DIR_MODELE.'/Page.php';

class PageController extends Controller
{
    /**
     * @param string $url
     * @return string
     */
    public function addAction($url) {
        try {
            $loader = new PageLoader($url);
            $this->assign('success', array('Page enregistrée'));
            Page::add($url, $loader->getTitle(), $loader->getDescription());
        }
        catch(Exception $e){
            $this->assign('errors', array($e->getMessage()));
        }

        $this->assign('pages', Page::all());
        return $this->fetch('index.tpl');
    }

    /**
     * @param int $id
     * @param string $title
     * @param string $description
     * @return string
     */
    public function updateAction($id, $title, $description) {
        Page::update($id, $title, $description);
        $this->assign('success', array('Modification enregistrée.'));
        $this->assign('pages', Page::all());
        return $this->fetch('index.tpl');
    }

    /**
     * @param int $id
     * @return string
     */
    public function deleteAction($id) {
        Page::delete($id);
        $this->assign('success', array('Suppression réussit.'));
        $this->assign('pages', Page::all());
        return $this->fetch('index.tpl');
    }

    /**
     * @return string
     */
    public function indexAction(){
        $this->assign('pages', Page::all());
        return $this->fetch('index.tpl');
    }
}