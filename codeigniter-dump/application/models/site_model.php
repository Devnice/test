<?php

/**
 * Class Site_model
 * Allows to interact with the table site in the database
 */
class Site_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    /**
     * Return all records in the database
     * @return array|false
     */
    public function all()
    {
        return $this->db->query('SELECT * FROM page')->result_array();
    }


    /**
     * Record siteweb in the database
     * @param string $url
     * @param string|null $title
     * @param string|null $description
     */
    public function add($url, $title, $description)
    {
        $this->db->query(
            'INSERT INTO page (url, title, description) VALUES (?, ?, ?)',
            array(
                $url,
                $title,
                $description
            )
        );
    }

    /**
     * Update siteweb in the database
     * @param int $id : Id record
     * @param string $url
     * @param string|null $title
     * @param string|null $description
     */
    public function update($id, $title, $description)
    {
        return $this->db->query(
            'UPDATE page
             SET title = ?,
             description = ?
             WHERE id = ? ',
            array(
                $title,
                $description,
                $id
            )
        );
    }

    /**
     * Remove record siteweb in the database
     * @param int $id : Id record
     * @return bool
     */
    public function delete($id){
        return $this->db->query('DELETE FROM page WHERE id = ? ', array($id));
    }
}