<?php
/**
 * Created by PhpStorm.
 * User: DEVNICE
 * Date: 6/2/2015
 * Time: 3:59 PM
 */
namespace AppBundle\Crawler;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Site
 * @package AppBundle\Crawler
 */
class Site {

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $description;


    /**
     * @param $url
     */
    public function __construct($url){
        $crawler = new Crawler(file_get_contents($url));
        $this->url = $url;

        $crawlerT = $crawler->filterXPath('//html/head/title');
        if($crawlerT->getNode(0)) $this->url = $crawlerT->text();

        $crawlerD = $crawler->filterXPath('//html/head/meta[@name="description"]');
        if($crawlerD->getNode(0)) $this->description = $crawlerD->attr('content');
    }


    public function getUrl(){
        return $this->url;
    }

    public function getTitle(){
        return $this->title;
    }

    public function getDescription(){
        return $this->description;
    }
}
