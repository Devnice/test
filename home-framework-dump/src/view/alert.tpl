{if !empty($errors)}
    {foreach $errors as $msg}
        <div class="alert alert-error">{$msg}</div>
    {/foreach}
{/if}
{if !empty($success)}
    {foreach $success as $msg}
        <div class="alert alert-success">{$msg}</div>
    {/foreach}
{/if}