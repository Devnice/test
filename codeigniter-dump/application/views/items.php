<?php
    if(false === empty($data)) {
        foreach ($data as $kdata => $vdata) {
?>
            <form method="post" action="#">
                <label for="title"><?php echo $vdata['url'] ?></label>
                <input type="hidden" name="id" value="<?php echo $vdata['id'] ?>">
                <input type="text" name="title" maxlength="100" value="<?php echo $vdata['title'] ?>">
                <textarea name="description" maxlength="200"><?php echo $vdata['description'] ?></textarea>
                <button class="btn-update" type="button" data-action="<?php echo base_url() ?>index.php/app/update">Modifier</button>
                <button class="btn-delete" type="button" data-action="<?php echo base_url() ?>index.php/app/delete">Supprimer</button>
            </form>
<?php
        }
    }
?>

<script type="text/javascript">
    // Event on the "delete" button
    var btnsDelete = document.getElementsByClassName('btn-delete');
    var nbBtnsDelete = btnsDelete.length;

    for(var i=0; i<nbBtnsDelete; i++){
        btnsDelete[i].addEventListener('click', function(){
            var form = this.parentNode;
            form.setAttribute('action', this.getAttribute('data-action'));
            form.submit();
        })
    }

    // Event on the "update" button
    var btnsUpdate = document.getElementsByClassName('btn-update');
    var nbBtnsUpdate = btnsUpdate.length;

    for(var i=0; i<nbBtnsUpdate; i++){
        btnsUpdate[i].addEventListener('click',  function(){
            var form = this.parentNode;
            form.setAttribute('action', this.getAttribute('data-action'));
            form.submit();
        })
    }
</script>